#include <stdio.h>

int main()
{
    char L ;
    printf("Enter a letter: ");
    scanf("%c", &L );
    if ( L=='A'||L=='E'||L=='I'||L=='O'||L=='U'||L=='a'||L=='e'||L=='i'||L=='o'||L=='u')
        printf("You entered a vowel letter");
    else
        printf("You entered a consonant");

    return 0;

}
